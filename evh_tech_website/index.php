<?php
//index.php

$error = '';
$name = '';
$email = '';
$subject = '';
$message = '';

function find_config($config_key) {
    $f = fopen("config.csv", "r");
    $result = false;
    while ($row = fgetcsv($f)) {
        if ($row[0] == $config_key) {
            $result = $row[1];
            break;
        }
    }
    fclose($f);
    return $result;
}

if(isset($_POST["submit"]))
{
	$contact_file_key;
	
	$name= $_POST['name'];
	$email= $_POST['email'];
	$subject= $_POST['subject'];
	$message= $_POST['message'];
 
	
	if($error == '')
	{
		// Read from config file to get contact file save path
		$contact_file_path = find_config('contact_file_path');
		
		// Now open the contact data file to write information
		$file_open = fopen($contact_file_path, "a");
		
		$no_rows = count(file($contact_file_path));
		if($no_rows > 1)
		{
			$no_rows = ($no_rows - 1) + 1;
		}
		$form_data = array(
			'sr_no'		=>	$no_rows,
			'name'		=>	$name,
			'email'		=>	$email,
			'subject'	=>	$subject,
			'message'	=>	$message
		);
		fputcsv($file_open, $form_data);
		$error = "<script>alert('Thank you for contacting us')</script>";
		
		$name = '';
$email = '';
$subject = '';
$message = '';
	}
}

?>
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from scripts.codeglamour.com/html/probizz/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 05 Jul 2018 09:05:26 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title> Leading Event Technology platform in India </title>
    <!--Favicon-->
    <link rel="shortcut icon" type="image/png" href="img/favicon.png">
    <!--Google Font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!--Bootstrap CSS-->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <!--Owl Carousel CSS-->
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <!--Slick CSS-->
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <!--Font Awesome CSS-->
    <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <!--Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!--Responsive CSS-->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    
     <link href="css/full-slider.css" rel="stylesheet">
    
</head>

<body>

   

    <!--Start Body Wrap-->
    <div id="body-wrapper">
        <!--Start Header-->
        <header id="header">
          

            <!--Start Main Menu-->
            <div class="main_navbar">   
            <!-- MAIN NAVBAR -->
                <nav class="navbar navbar-expand-lg  navbar-dark">
                    <div class="container">
                        <a class="navbar-brand logo-sticky font-600" href="index.php">  <img src="img/eh-tech-logo.png" class="img-fluid"  width="355" height="88">   </a>
                        <button class="navbar-toggler collapsed" data-toggle="collapse" data-target="#navbarNav" aria-expanded="false"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav ml-auto" id="nav">
                                <li class="nav-item">
                                    <a class="nav-link js-scroll-trigger" href="#about">Who we are</a>
                                </li>
                                 <li class="nav-item">
                                    <a class="nav-link js-scroll-trigger" href="#services">What we Do</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link js-scroll-trigger" href="#portfolios">Resources</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link js-scroll-trigger" href="careers.php" target="_blank">Careers</a>
                                </li>
                               
                                <li class="nav-item">
                                    <a class="nav-link js-scroll-trigger" href="#contact">Contact us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <!--End Main Menu-->

        </header>
        <!--End Header-->

      
     
        <div class="caption-content dp-table" >
                <div class="tbl-cell position-relative">
                   
                    <h1 class="color-white-top"> EMPOWER YOUR EVENT WITH TECHNOLOGY   <BR>
                     
                     <p style="color:#FFFFFF; font-family: open sans; width: 100%;">We fulfill your event needs by providing cutting edge, best and customized technology solutions</p>
                    
                    </h1>
                    
                 
                    <div class="large-btn animated slideInUp" style="text-align: center;"> 
                        <a class="btn btn-light mt-5 mr-4"  href="#about">Explore About Us</a>
                    </div>
                </div>
            </div>
     
     
       <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
       
        
        
        
        <div class="carousel-inner" role="listbox">
         
         
         
          <!-- Slide One - Set the background image for this slide in the line below -->
          <div class="carousel-item active" style="background-image: url('img/slider-1.jpg')">
             
          </div>
          
          
          
          <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item"  style="background-image: url('img/slider-2.jpg')">
            
          </div>
          <!-- Slide Three - Set the background image for this slide in the line below -->
          <div class="carousel-item"  style="background-image: url('img/slider-3.jpg')">
          
          </div>
        </div>
      
       
      </div>

        <!--Start About Section-->
        <section id="about" >
            <!--Start Container-->
            <div class="container">
                <!--Start Row-->
                <div class="row">
                    <!--Start About Image-->
                    <div class="col-lg-6">
                        <div class="about-img" style="margin-top: 50px;">
                            <img src="img/about_us.png" class="img-fluid" alt="Image">
                        </div>
                    </div>
                    <!--End About Image-->

                    <!--Start About Content-->
                    <div class="col-lg-6">
                        <div class="about-content">
                            <h4 class="color-gray" style="padding: 10px; background-color:#f6c82e; width:35%; margin:0px;">Who We Are</h4>
                            <h2>We are on a Mission  </h2>
                            <p s>Technology has revolutionized the Event industry, changing how conferences and corporate events are planned and executed, what they look like, what attendees do on site, how they are evaluated, and much more. EVH Technologies brings the most advanced technology and innovative solutions for your conferences and events which will help you to organize your events more efficiently, enhance attendees’ experiences, adding value and create a wow factor!</p>
                            <p style="padding-bottom: 35px;" > EVH Technologies is a young brainchild of passionate entrepreneurs, technocrats and industry stalwarts. We automate systems and committed to provide best technology solutions and ecommerce advantage to the wide range of businesses. We strive to become a leading technology and solutions company in wide range of business domains. </p>
                       
                        </div>
                    </div>
                    <!--End About Content-->
                </div>
                <!--End Row-->
            </div>
            <!--End Container-->
        </section>
        <!--End About Section-->

        <!--Start Services Section-->
        <section id="services"  class="bg-gray">
            <!--Start Container-->
            <div class="container">
                <!--Start Heading-->
                <div class="row">
                    <div class="col-12">
                        <div class="heading text-center">
                            <h4 class="color-gray" style="padding: 10px; background-color:#f6c82e; width:35%;">What We Do</h4>
                            
                            <p style="font-size:24px; line-height: 35px;">We automate systems and committed to provide best technology solutions and ecommerce advantage to the wide range of businesses. </p>
                        </div>
                    </div>
                </div>
                <!--End Heading-->

                <!--Start Services Row-->
                <div class="row">
                    <!--Start Service Single-->
                    <div class="col-lg-4 col-md-6">
                        <div class="service-single text-center">
                            <img src="img/event_technology.png" alt="icon">
                            <h4 >EVENT TECHNOLOGY</h4>
                            <p>From essentials to latest and cutting edge, plethora of innovative technologies, tools and solutions for a unique and memorable event experience </p>
                        </div>
                    </div>
                    <!--End Service Single-->

                    <!--Start Service Single-->
                    <div class="col-lg-4 col-md-6">
                        <div class="service-single text-center">
                             <img src="img/customized-event.png" alt="icon">
                            <h4>CUSTOMIZED EVENT APPLICATIONS</h4>
                            <p>Tell us what you need, and our team of developers, designers, technocrats and project management will deliver it exceeding your expectations </p>
                        </div>
                    </div>
                    <!--End Service Single-->

                    <!--Start Service Single-->
                    <div class="col-lg-4 col-md-6">
                        <div class="service-single text-center">
                             <img src="img/strategic-application.png" alt="icon">
                            <h4 >STRATEGIC ENGAGEMENT</h4>
                            <p>From digital marketing to brand management and reaching out to your target audience with a solid ROI, we have all customized solutions best fitting to your needs. </p>
                        </div>
                    </div>
                    <!--End Service Single-->

                    <!--Start Service Single-->
                    <div class="col-lg-4 col-md-6">
                        <div class="service-single text-center">
                             <img src="img/mobile-apps.png" alt="icon">
                            <h4>MOBILE APPLICATION</h4>
                            <p>Exclusive apps which have great utility. Awesome features which will ensure that your attendees not only download your event app but also use them!</p>
                        </div>
                    </div>
                    <!--End Service Single-->

                    <!--Start Service Single-->
                    <div class="col-lg-4 col-md-6">
                        <div class="service-single text-center">
                             <img src="img/payment-integration-solutions.png" alt="icon">
                            <h4 >PAYMENT INTEGRATION AND SOLUTIONS</h4>
                            <p>Online payment, reconciliation, accounting and easy of transaction solutions with highest security and reliability features </p>
                        </div>
                    </div>
                    <!--End Service Single-->

                    <!--Start Service Single-->
                    <div class="col-lg-4 col-md-6">
                        <div class="service-single text-center">
                             <img src="img/strategic-planning.png" alt="icon">
                            <h4 >STRATEGIC PLANNING & CONSULTING</h4>
                            <p>Strategic Event Planning, expert consultation, execution, checklists and much more </p>
                        </div>
                    </div>
                    <!--End Service Single-->
                </div>
                <!--End Services Row-->


            </div>
            <!--End Container-->
        </section>
        <!--End Services Section-->

   
    

        <!--Start Why Choose Us-->
        <section id="portfolios">
            <!--Start Container-->
            <div class="container">
                <!--Start Row-->
                <div class="row">
                    <!--Start Why Choose Content-->
                    <div class="col-lg-6">
                        <div class="why-us-content">
                            <h4 class="color-gray" style="padding: 10px; background-color:#f6c82e; width:35%; margin:0px;">Resources</h4>
                            <h2> Explore the latest information on event technology </h2>
                           
                            <div class="why-us-features row">
                                <div class="col-md-12">
                                    <ul>
                                        <li><i class="fas fa-check"></i> <a href="https://www.sli.do" target="_blank"> Audience interaction tool for meetings, events and conferences </a> </li>
                                        <li><i class="fas fa-check"></i> <a href="https://catchbox.com" target="_blank"> Make your next Conference, Meeting or Lecture more engaging and fun </a>  </li>
                                        <li><i class="fas fa-check"></i> <a href="http://mashmachines.com/" target="_blank" > Unique experience with music designed to engage and bring people together in events </a>   </li>
                                        <li><i class="fas fa-check"></i> <a href="https://www.noodlelive.com/blog/event-technology-trends-to-watch-in-2018/"  target="_blank">  Event technology trends to watch in 2018 </a>  </li>
                                        
                                           <li><i class="fas fa-check"></i> <a href="https://www.eventmanagerblog.com/interactive-technology-ideas"  target="_blank"> 	Incorporate Interactive Technologies into Your Event  </a>  </li>
                                    </ul>
                                        
                                    </ul>
                                </div>
                              
                            </div>
                        </div>
                    </div>
                    <!--End Why Choose Content-->

                    <!--Start Why Choose Image-->
                    <div class="col-lg-6">
                        <div class="why-us-img">
                            <img src="img/why-us-bg.jpg" class="img-fluid" alt="Image">
                        </div>
                    </div>
                    <!--End Why Choose Image-->
                </div>
                <!--End Row-->
            </div>
            <!--End Container-->
        </section>
        <!--End Why Choose Us-->

  

     
    
    

        <!-- testimonial Section Start -->
    
    
    <!-- testimonial Section Start -->


        <!--Start Contact Section-->
        <section id="contact" class="default-padding bg-gray">
            <!--Start Container-->
            <div class="container">
                <!--Start Heading-->
                <div class="row">
                    <div class="col-12">
                        <div class="heading text-center">
                           
                            <h2 >Connect With Us </h2>
                            <h2 style="font-size:30px;  text-transform: capitalize;"> Get the Power of Technology </h2>
                        </div>
                    </div>
                </div>
                <!--End Heading-->

                <!--Start Contact Row-->
                <div class="row">
                    <!--Start Contact Info-->
                    <div class="col-lg-6">
                        <div class="contact-info">
                            <!--Start Contact Info Single-->
                            <div class="row">
                                <div class="col-lg-12 col-sm-12">
                                    <div class="contact-info-single text-center">
                                       <i class="fa fa-home"></i>
                                        <p  style="font-size:19px; line-height: 30px;"> <img src="img/eh-tech-logo-bottom.png"  class="img-fluid" width="385" height="109" >  <br>
B 313, 3rd Floor, Ansal Chamber-1, <br> 
BhikajiCama Place, <br>
New Delhi 110066<br>
<br>
Phone: <strong>+91-11-40197935</strong>  <br>


E-mail: <a href="mailto:connect@evhtechnologies.com" style="font-size: 21px;">connect@evhtechnologies.com</a>

</p>






</div>


<div class="mapouter"><div class="gmap_canvas">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d876.0074156650702!2d77.18821342916722!3d28.56887197671864!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1d08b53de3c1%3A0xa95688be413dab6c!2sEVH+Technologies!5e0!3m2!1sen!2sus!4v1531895949245" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div><style>.mapouter{text-align:right;height:250px;width:auto;}.gmap_canvas {overflow:hidden;background:none!important;height:250px;width:auto;}</style></div>


 <!--End Contact Info Single-->
</div>
                               
                              
                            
                            </div>
                        </div>
                    </div>
                    <!--End Contact Info-->

                    <!--Start Contact Form-->
                    <div class="col-lg-6">
                        <div class="contact-form">
                            <form method="post" onsubmit="return validate()" id="submit_form" name="myForm">
							
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Name*"  id="name" name="name" value="<?php echo $name; ?>" >
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email*"  id="email" name="email" value="<?php echo $email; ?>" >
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Subject*"  id="subject" name="subject" value="<?php echo $subject; ?>">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="10" placeholder="Write Your Message*"  id="message" name="message" value="<?php echo $message; ?>"></textarea>
                                </div>
                                <div class="contact-btn text-center" style="display:inline;">
									
                                    <button type="submit" id="submit" name="submit">Submit</button>
									<p style="color:red;display:inline;" id="error_message"></p>
                                </div>
                            </form>
                            <div id="form-messages"></div>
                        </div>
                    </div>
                    <!--End Contact Form-->
                </div>
                <!--End Contact Row-->
            </div>
            <!--End Container-->
        </section>
        <!--End Contact Section-->

        <!--Start Footer-->
        <footer id="footer">
            <!--Start Footer Top-->
            <div class="footer-top">
                <!--Start Container-->
                <div class="container">
                    <!--Start Row-->
                    <div class="row">
                        <!--Start Footer About-->
                        <div class="col-lg-3 col-md-6 mb-3">
                            <div class="footer-about">
                                <h3 class="color-white" style="text-align:left;">About Us</h3>
                                <p> EVH Technologies is a young brainchild of passionate entrepreneurs, technocrats and industry stalwarts. We automate systems and committed to provide best technology solutions and ecommerce advantage to the wide range of businesses. We strive to become a leading technology and solutions company in wide range of business domains. 
                                
                                <a href="javascript:void(0);">Read More...</a>
                                
                                </p>
                            </div>
                          
                        </div>
                        <!--End Footer About-->

                        <!--Start Footer Links-->
                        <div class="col-lg-3 col-md-6 mb-3">
                            <div class="footer-links">
                                <h3 class="color-white" style="text-align:left;">Important Links</h3>
                                <ul>
                                    <li><a href="javascript:void(0);"><i class="fas fa-angle-right"></i>  Our Services </a>  </li>
                                    <li><a href="javascript:void(0);"><i class="fas fa-angle-right"></i> Resources </a></li>
                                    <li><a href="javascript:void(0);"><i class="fas fa-angle-right"></i> Current Openings </a></li>
                                    
                                </ul>
                            </div>
                        </div>
                        <!--End Footer Links-->

                        <!--Start Footer Latest News-->
                        <div class="col-lg-3 col-md-6 mb-3">
                            <div class="footer-latest-news">
                                <h3 class="color-white" style="text-align:left;">Latest News</h3>
                                <!--Start Recent Post Single-->
                                <div class="recent-post-single fix">
                                   
                                    <div class="post-cont float-left">
                                        <h5><a class="color-white" href="javascript:void(0);"> Latest News 1 </a></h5>
                                      
                                    </div>
                                </div>
                                <!--End Recent Post Single-->

                                <!--Start Recent Post Single-->
                                <div class="recent-post-single fix">
                                   
                                    <div class="post-cont float-left">
                                        <h5><a class="color-white" href="javascript:void(0);"> Latest News 2</a></h5>
                                        
                                    </div>
                                </div>
                                <!--End Recent Post Single-->

                                <!--Start Recent Post Single-->
                                <div class="recent-post-single fix">
                                    
                                    <div class="post-cont float-left">
                                        <h5><a class="color-white" href="javascript:void(0);"> Latest News 3</a></h5>
                                       
                                    </div>
                                </div>
                                <!--End Recent Post Single-->
                            </div>
                        </div>
                        <!--End Footer Latest News-->

                        <!--Start Footer Instagram-->
                        <div class="col-lg-3 col-md-6 mb-3">
                           <div class="footer-social-area">
                                <h3 class="color-white" style="text-align:left;">Follow Us</h3>
                                <p class="mb-3">We are happy to see you here.</p>
                                <ul>
                                    <li><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="javascript:void(0);"><i class="fab fa-pinterest-p"></i></a></li>
                                    <li><a href="javascript:void(0);"><i class="fab fa-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <!--End Footer Instagram-->

                    </div>
                    <!--End Row-->
                </div>
                <!--End Container-->
            </div>
            <!--End Footer Top-->

            <!--Start Footer Bottom-->
            <div class="footer-bottom">
                <p class="color-white text-center"> &copy; Copy 2018. All Rights Reserved By EH Technology.</p>
            </div>
            <!--End Footer Bottom-->

            <!--Start Click To Top-->
            <div class="click-to-top">
                <a href="#body-wrapper" class="js-scroll-trigger"><i class="fas fa-angle-double-up"></i></a>
            </div>
            <!--End Click To Top-->
        </footer>
        <!--End Footer-->
    </div>
    <!--End Body Wrap-->

    <!--jQuery JS-->
    <script src="js/jquery-min.js"></script>
    <!--Custom Google Map JS-->
    <script src="js/map-scripts.js"></script>
    <!--Slick Js-->
    <script src="js/slick.min.js"></script>
    <!--Counter JS-->
    <script src="js/waypoints.js"></script>
    <script src="js/counterup.min.js"></script>
    <!--Bootstrap JS-->
    <script src="js/bootstrap.min.js"></script>
    <!--Magnic PopUp JS-->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/mixitup.min.js"></script>
    <!--Owl Carousel JS-->
    <script src="js/owl.carousel.min.js"></script>
    <!--Jquery Easing Js -->
    <script src="js/jquery.easing.js"></script>
    <!--Scrolly JS-->
    <script src="js/scrolly.js"></script>
    <!--Ajax Contact JS-->
    <script src="js/ajax-contact-form.js"></script>
    <!--Main JS-->
    <script src="js/custom.js"></script>
    
    
    
    
    
    
        <!-- Bootstrap core JavaScript -->

    
    
    

</body>


<!-- Mirrored from scripts.codeglamour.com/html/probizz/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 05 Jul 2018 09:06:34 GMT -->
</html>
<script>
   function validate()
   {    var name = document.forms["myForm"]["name"].value;
	    var email = document.forms["myForm"]["email"].value;
	    var subject = document.forms["myForm"]["subject"].value;
	    var message = document.forms["myForm"]["message"].value;
	    
	  
		if (name == "") {
			//$('#error_message').attr("Please enter your name!!");
			document.getElementById('error_message').innerHTML = "Please enter your name!!";
			return false;
    }
	   else if(email == '')
	   {
		
		   document.getElementById('error_message').innerHTML = "Please enter your Email Id!!";
		   return false;
	   }
	   else if(subject =='')
	   {
		 
		   document.getElementById('error_message').innerHTML = "Please enter Subject!!";
		   return false;
	   }
	   else if(message =='')
	   {
		  
		   document.getElementById('error_message').innerHTML = "Please enter Message!!";
		   return false;
	   }
	   else if(name != "" && email != ''&& subject != ''&& message != '')
	   {
		   alert("Thanks for contacting us");
		   
	   }
   }
   if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>
