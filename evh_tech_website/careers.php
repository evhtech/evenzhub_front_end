<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from scripts.codeglamour.com/html/probizz/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 05 Jul 2018 09:05:26 GMT -->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<title> Leading Event Technology platform in India </title>
	<!--Favicon-->
	<link rel="shortcut icon" type="image/png" href="img/favicon.png">
	<!--Google Font-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<!--Bootstrap CSS-->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<!--Owl Carousel CSS-->
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
	<!--Slick CSS-->
	<link rel="stylesheet" type="text/css" href="css/slick.css">
	<!--Font Awesome CSS-->
	<link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
	<!--Main CSS-->
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<!--Responsive CSS-->
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<link href="css/full-slider.css" rel="stylesheet">


	<style>
		.product {
			width: 350px;
			margin: 0 auto;
		}
		
		ul li {
			line-height: 35px;
		}
	</style>

</head>

<body>



	<!--Start Body Wrap-->
	<div id="body-wrapper">
		<!--Start Header-->

		<div class="product">
			<a href="http://www.evhtechnologies.com/" target="_blank">
  <img src="img/eh-tech-logo-bottom.png" class="img-fluid"  width="385" height="109">
  
  </a>
		
		</div>



		<div class="carousel-inner" role="listbox">



			<!-- Slide One - Set the background image for this slide in the line below -->
			<div class="carousel-item active" style="background-image: url('img/banner-2.jpg')">

				<div class="caption-content dp-table">


					<div class="tbl-cell position-relative">

						<h1 class="color-white" style="font-size: 40px; font-weight: 200; background-color:#029fd7; padding: 20px;  font-family: open sans;">JOIN OUR PASSIONATE TEAM  OF YOUNG INNOVATORS   <BR>
                     
                     <p style="color:#FFFFFF; font-family: open sans; width: 100%; line-height:40px;">If you want to join something that is much more than a day job and think you’ve got some awesome things about you -
                      <strong style="font-size:30px;">Go ahead </strong>, <strong style="font-size:30px;">Apply away!</strong></p>
                     
                     
                    </h1>


					</div>
				</div>



			</div>



			<div class="container">

				<div class="row">
					<div class="col-sm-12">

						<h1 style="font-size:35px; font-family: open sans; margin-top: 25px; font-weight: bold;">  Current Openings  </h1>

						<ul style="margin:20px 0px 35px 0;">


							<li> Assistant Manager/Manager (Venues & MICE) – 1 Nos. - <a href="./pdf/JD AM-Hotel and Venues.pdf" target="_blank" class="click_here">Click here for JD </a> </li>


							<li> Assistant Manager/Manager – Digital Marketing – 1 Nos. - <a href="./pdf/JD Digital Marketing Manager.pdf" target="_blank" class="click_here">Click here for JD </a> </li>


							<li> Event Technology Associate – 1 Nos. - <a href="./pdf/JD Event Technology FV.pdf" target="_blank" class="click_here"> Click here for JD </a> </li>

							<li> Sales Manager (Events and Conferences) – 1 Nos. – <a href="./pdf/JD Sales Manager.pdf" target="_blank" class="click_here">Click here for JD </a> </li>


							<li> Sr. Software Engineer - PHP – 1 Nos. - <a href="./pdf/JD-SSE PHP.pdf" target="_blank" class="click_here">Click here  for JD </a>
							</li>

						</ul>


					</div>



				</div>

			</div>







		</div>


	</div>










	<!-- testimonial Section Start -->


	<!-- testimonial Section Start -->



	<!--Start Footer-->
	<footer id="footer">
		<!--Start Footer Top-->
		<div class="footer-top">
			<!--Start Container-->
			<div class="container">
				<!--Start Row-->
				<div class="row">
					<!--Start Footer About-->
					<div class="col-lg-3 col-md-6 mb-3">
						<div class="footer-about">
							<h3 class="color-white" style="text-align:left;">About Us</h3>
							<p> EVH Technologies is a young brainchild of passionate entrepreneurs, technocrats and industry stalwarts. We automate systems and committed to provide best technology solutions and ecommerce advantage to the wide range of businesses. We strive to become a leading technology and solutions company in wide range of business domains.

								<a href="#about">Read More...</a>

							</p>
						</div>

					</div>
					<!--End Footer About-->

					<!--Start Footer Links-->
					<div class="col-lg-3 col-md-6 mb-3">
						<div class="footer-links">
							<h3 class="color-white" style="text-align:left;">Important Links</h3>
							<ul>
								<li><a href="javascript:void(0);"><i class="fas fa-angle-right"></i>  Our Services </a> </li>
								<li><a href="javascript:void(0);"><i class="fas fa-angle-right"></i> Resources </a>
								</li>
								<li><a href="javascript:void(0);"><i class="fas fa-angle-right"></i> Current Openings </a>
								</li>

							</ul>
						</div>
					</div>
					<!--End Footer Links-->

					<!--Start Footer Latest News-->
					<div class="col-lg-3 col-md-6 mb-3">
						<div class="footer-latest-news">
							<h3 class="color-white" style="text-align:left;">Latest News</h3>
							<!--Start Recent Post Single-->
							<div class="recent-post-single fix">

								<div class="post-cont float-left">
									<h5><a class="color-white" href="javascript:void(0);"> Latest News 1 </a></h5>

								</div>
							</div>
							<!--End Recent Post Single-->

							<!--Start Recent Post Single-->
							<div class="recent-post-single fix">

								<div class="post-cont float-left">
									<h5><a class="color-white" href="javascript:void(0);"> Latest News 2</a></h5>

								</div>
							</div>
							<!--End Recent Post Single-->

							<!--Start Recent Post Single-->
							<div class="recent-post-single fix">

								<div class="post-cont float-left">
									<h5><a class="color-white" href="javascript:void(0);"> Latest News 3</a></h5>

								</div>
							</div>
							<!--End Recent Post Single-->
						</div>
					</div>
					<!--End Footer Latest News-->

					<!--Start Footer Instagram-->
					<div class="col-lg-3 col-md-6 mb-3">
						<div class="footer-social-area">
							<h3 class="color-white" style="text-align:left;">Follow Us</h3>
							<p class="mb-3">We are happy to see you here.</p>
							<ul>
								<li><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a>
								</li>
								<li><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a>
								</li>
								<li><a href="javascript:void(0);"><i class="fab fa-pinterest-p"></i></a>
								</li>
								<li><a href="javascript:void(0);"><i class="fab fa-linkedin-in"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<!--End Footer Instagram-->

				</div>
				<!--End Row-->
			</div>
			<!--End Container-->
		</div>
		<!--End Footer Top-->

		<!--Start Footer Bottom-->
		<div class="footer-bottom">
			<p class="color-white text-center"> &copy; Copy 2018. All Rights Reserved By EH Technology.</p>
		</div>
		<!--End Footer Bottom-->

		<!--Start Click To Top-->
		<div class="click-to-top">
			<a href="#body-wrapper" class="js-scroll-trigger"><i class="fas fa-angle-double-up"></i></a>
		</div>
		<!--End Click To Top-->
	</footer>
	<!--End Footer-->
	</div>
	<!--End Body Wrap-->

	<!--jQuery JS-->
	<script src="js/jquery-min.js"></script>
	<!--Custom Google Map JS-->
	<script src="js/map-scripts.js"></script>
	<!--Slick Js-->
	<script src="js/slick.min.js"></script>
	<!--Counter JS-->
	<script src="js/waypoints.js"></script>
	<script src="js/counterup.min.js"></script>
	<!--Bootstrap JS-->
	<script src="js/bootstrap.min.js"></script>
	<!--Magnic PopUp JS-->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/mixitup.min.js"></script>
	<!--Owl Carousel JS-->
	<script src="js/owl.carousel.min.js"></script>
	<!--Jquery Easing Js -->
	<script src="js/jquery.easing.js"></script>
	<!--Scrolly JS-->
	<script src="js/scrolly.js"></script>
	<!--Ajax Contact JS-->
	<script src="js/ajax-contact-form.js"></script>
	<!--Main JS-->
	<script src="js/custom.js"></script>






	<!-- Bootstrap core JavaScript -->





</body>


<!-- Mirrored from scripts.codeglamour.com/html/probizz/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 05 Jul 2018 09:06:34 GMT -->

</html>